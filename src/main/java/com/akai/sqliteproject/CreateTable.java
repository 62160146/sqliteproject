/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.akai.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ii drunkboy
 */
public class CreateTable {
    public static void main(String[] args) {
      Connection conn = null;
      Statement stmt = null;
      String dbName = "user.db";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:"+dbName);
            stmt = conn.createStatement();
            String sql = "CREATE TABLE COMPANY" + 
                    "(ID INT PRIMARY KEY NOT NULL," + 
                    "NAME TEXT NOT NULL," + 
                    "AGE INT NOT NULL," + 
                    "ADDRESS CHAR(50)," +
                    "SALARY REAL)";
            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("No Library org.sqlite.JDBC");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Opened databse successfully");
            System.exit(0);
        }
        
        
      
    }
}
